package util;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ElementActions {
public WebDriver driver;
public WebDriverWait wait;
public Actions act;
public JavascriptExecutor js;
public ElementActions(WebDriver driver){
	this.driver=driver;
	wait=new WebDriverWait(driver,20);
	act=new Actions(driver);
	js=(JavascriptExecutor)driver;
	
	
}
public enum dropdownType{
BY_VISIBLETEXT,BY_VALUE,BY_INDEX;	
}
public void clickOn(By locator){
	wait.until(ExpectedConditions.elementToBeClickable(locator));
	driver.findElement(locator);
}
public void movetoElement(By locator){
	waitInseconds(4);
	WebElement ele=driver.findElement(locator);
	act.moveToElement(ele).build().perform();	
}
public void scrollTillelement(By locator){
	wait.until(ExpectedConditions.presenceOfElementLocated(locator));
	js.executeScript("arguments[0].scrollIntoView", driver.findElement(locator));
	
}
public void waitInseconds(int sec){
	
	try {
		Thread.sleep(sec * 4000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
public void selectdropdowntype(By locator,dropdownType type,Object value){
Select sel=new Select(driver.findElement(locator));	
switch(type){
case BY_INDEX:
int index=(Integer)value;
sel.selectByIndex(index);
break;
case BY_VISIBLETEXT:
	String str=(String)value;
	sel.selectByVisibleText(str);
	break;
case BY_VALUE:
	String val=(String)value;
	sel.selectByValue(val);
}
	
}
}
