package base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;

import util.Configuration;

public class BaseClass {
	public static WebDriver driver;
	@BeforeSuite
	public void suitsetup(){
		Configuration.loadconfiguration();
		
	}
	@BeforeMethod
public void launchbrowser(){
	String browsername=Configuration.getbrowsername();
	switch(browsername){
	case "chrome":
	System.setProperty("webdriver.chrome.driver", "binaries/chromedriver.exe");
	driver=new ChromeDriver();
	}
	driver.manage().window().maximize();
	driver.get(Configuration.geturl());
}
}
